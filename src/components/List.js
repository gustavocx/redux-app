import React, { useEffect } from "react";
import { connect } from "react-redux";
import Item from "./Item";
import { fetchData, store_all } from "../redux/actions/apiActions";

const List = ({ list, loading, error, fetchData }) => {
  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <div className="List">
      <h1>List</h1>
      {loading ? "Cargando datos ..." : ""}
      {error ? "Error: " + error : ""}
      {list && list.length
        ? list.map((item, index) => {
            return <Item data={item} key={index} />;
          })
        : ""}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    list: state.apiState.list,
    loading: state.apiState.loading,
    error: state.apiState.error,
  };
};

export default connect(mapStateToProps, { store_all, fetchData })(List);
