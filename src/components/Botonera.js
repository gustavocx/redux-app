import React, { useEffect } from "react";
import { nav_click } from "../redux/actions/navActions";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Home from "./Home";
import List from "./List";
import New from "./Form";

const Botonera = ({ title, nav_click, match: { params } }) => {
  const buttonClick = (_title) => {
    nav_click({
      title: _title,
    });
  };

  useEffect(() => {
    let title = "Redux App";
    switch (params.section) {
      case "home":
        title = "Home";
        break;
      case "list":
        title = "List";
        break;
      case "new":
        title = "New";
        break;
      default:
        break;
    }
    nav_click({
      title: title,
    });
  }, [nav_click, params.section]);

  const renderSection = () => {
    switch (params.section) {
      case "home":
        return <Home />;
      case "list":
        return <List />;
      case "new":
        return <New />;
      default:
        return;
    }
  };

  return (
    <div className="Botonera">
      <header className="AppHeader">
        <div>
          <h1>{title}</h1>
        </div>
        <Link
          to="home"
          onClick={() => {
            buttonClick("Home");
          }}
        >
          Home
        </Link>
        <Link
          to="list"
          onClick={() => {
            buttonClick("List");
          }}
        >
          List
        </Link>
        <Link
          to="new"
          onClick={() => {
            buttonClick("New");
          }}
        >
          New
        </Link>
      </header>
      {renderSection()}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return { title: state.navState.title, section: ownProps.section };
};

export default connect(mapStateToProps, { nav_click })(Botonera);
